/*
The following is a partial version of the "game" object I'm working on.
This object will interface with the game providing a shared script framework
on which other scripts can rely on for accuracy and less redudant code.
*/
var game = {
    defines: {
        affinities: {
            Warror: 1,
            Rouge: 2,
            Ranger: 3,
            Evoker: 4,
        },
        masteries: {
            Mining: 1,
            Forestry: 2,
            Fishing: 3,
            Botany: 4,
            Enchanting: 5,
            Cooking: 6,
            Blacksmithing: 7,
            Woodworking: 8,
            Alchemy: 9,
            Tailoring: 10,
        },
    },
    common: {
        //much of the games responses are wrapped in javascript, and escaped.
        //this will parse the html and remove all escaping from the javascript,
        //allowing it to be used to build a json array.
        parseHtml: function(line_expected, response) {

            //split the response into seperate lines.
            //each line is a javascript command... line_expected should be
            //the line that the .html() call to parse appears on.
            response = response.split("\n")[line_expected-1];

            //remove the javascript wrappers on this call.
            response = response.split("html(")[1];
            response = response.substr(1, response.length-4);

            //replace escaping text with empty strings.
            response = response.replace(/\\n/g,"");
            response = response.replace(/\\/g,"");

            //return the response as text to the requestor.
            return response;
        },
        parseInt: function(data) {
            if(data === null) {
                return 0;
            }
            return parseInt(data.replace(/[^0-9.]/g, ''));
        }
    },
    inventory: {
        getComponents: function(name = "", stat = "", min_quality = 1, max_quality = 20, callback) {

        },
        getEquipment: function(callback) {

        },
        getItems: function(name = "", item_type = "", callback) {

        },
        destoryEquipment: function(equipment_id, callback) {
            
        },
        equipEquipment: function(equipment_id, callback) {
            
        },
        destoryComponent: function(component_id, callback) {
            
        }
    },
    leaderboards: {

    },
    locations: {

    },
    market: {
        //returns a json array of orders user has on market.
        getMyListings: function(callback) {
            var url = "/trade/1/my_listings";
            var request = $.ajax({
                method: "POST",  
                url: url,
            });
            
            request.done(function(response) {
                if(typeof callback === "function")
                {
                    var html = $("[id*=listing]", game.common.parseHtml(1, response));
                    var data = {
                        listings: [],
                    };

                    for(var i = 0; i < html.length; i++) {
                        var columns = $("td", html[i]);
                        
                        var offer_type = columns[0].innerText.trim();
                        var listing_id = game.common.parseInt(html[i].id);
                        var name = "";
                        var stats = columns[2].innerText.replace("Stats:","").trim();
                        var cost = game.common.parseInt(columns[3].innerText);

                        if(offer_type == "Component Sale") {
                            name = columns[1].innerText;

                        }
                        else if(offer_type == "Equipment Sale") {
                            name = columns[1].childNodes[columns[1].childNodes.length-1].nodeValue.trim();

                        }

                        data.listings.push({
                            listing_id: listing_id,
                            offer_type: offer_type,
                            name: name,
                            stats: stats,
                            cost: cost,
                        });
                    }


                    callback(data);
                }
            });
        },

        //cancels the order that user has on market
        cancelComponentSale: function(order_id) {
            var url = "/trade/1/cancel_component_order";
            var request = $.ajax({
                method: "POST",  
                url: url,
                data: {
                    "order_id": order_id
                },
            });
        },

        //posts a list of components to the market for the price provided.
        postComponentSale: function(component_ids, price){
            var url = "/trade/1/post_component_sale";
            var request = $.ajax({
                method: "POST",  
                url: url,
                data: {
                    "component-cost": price,
                    "component-select[]": component_ids
                },
            });
        },

    },
    profile: {

    },
}